## static list integration

Some schools upload the plan at some specific, "never" changing URLs.
This allows to use such plans in the vertretungsplan.io application.

The content list (list of files and the metadata) is served from this application.
The content files (plan HTMLs, PDFs, ...) are served directly from the server of the schools.

Schools are managed in the source code at ``src/config/input.ts``.
The listening port (default: 8080) can be set using the ``PORT`` environment variable.

### License

AGPL 3.0

> vertretungsplan.io static list integration
> Copyright (C) 2019 - 2022 Jonas Lochmann
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the GNU Affero General Public License as
> published by the Free Software Foundation, version 3 of the
> License.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU Affero General Public License for more details.
>
> You should have received a copy of the GNU Affero General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.
