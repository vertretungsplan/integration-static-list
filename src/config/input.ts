/*
 * vertretungsplan.io static list integration
 * Copyright (C) 2019 - 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Config } from './schema.js'

export const config: Config = {
  port: process.env.PORT || 8080,
  schools: [
    {
      type: 1,
      id: 'hhs-gadebusch',
      title: 'Heinrich-Heine-Schule Gadebusch',
      files: [
        {
          id: 'plan',
          title: 'aktueller Plan',
          url: 'http://heinrich-heine-schule-gadebusch.de/Vertretung/Vertretungsplan.html',
          type: 'plan'
        }
      ]
    },
    {
      type: 1,
      id: 'jwg-heidenau',
      title: 'Oberschule "Johann Wolfgang von Goethe" Heidenau',
      files: [
        {
          id: 'plan',
          title: 'aktueller Plan',
          url: 'https://www.sachsen.schule/~goethe-ms-heidenau/includes/vertretung.pdf',
          type: 'plan'
        }
      ]
    },
    {
      type: 1,
      id: 'rgs-spenge',
      title: 'Regenbogen-Gesamtschule Spenge',
      files: [
        {
          id: 'plan1',
          title: 'Aktueller Schultag',
          url: 'http://plan.regenbogen-gesamtschule.de/vertretung/schueler/subst_001.htm',
          type: 'plan'
        },
        {
          id: 'plan2',
          title: 'Folgender Schultag',
          url: 'http://plan.regenbogen-gesamtschule.de/vertretung/schueler/subst_002.htm',
          type: 'plan'
        }
      ]
    },
    {
      type: 1,
      id: 'zg-gera',
      title: 'Zabel-Gymnasium Gera',
      files: [
        {
          id: 'plan',
          title: 'aktueller Plan',
          url: 'https://zabel-gymnasium.de/vertretungsplan/aktueller_plan/VP_Internet.pdf',
          type: 'plan'
        }
      ]
    },
    {
      type: 1,
      id: 'gg-lb',
      title: 'Goethe-Gymnasium Ludwigsburg',
      files: [
        {
          id: 'today',
          title: 'Heute',
          url: 'https://www.goethelb.de/files/untis/heute.html',
          type: 'plan'
        },
        {
          id: 'tomorrow',
          title: 'Morgen',
          url: 'https://www.goethelb.de/files/untis/morgen.html',
          type: 'plan'
        }
      ]
    }
  ]
}
