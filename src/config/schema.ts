/*
 * vertretungsplan.io static list integration
 * Copyright (C) 2019 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

export interface Config {
  port: number | string
  schools: Array<SchoolItem>
}

export type SchoolItem = SchoolItemType1 | SchoolItemType2

export interface SchoolItemType1 {
  type: 1
  id: string
  title: string
  files: Array<FileItem>
}

export type SchoolItemType2 = {
  type: 2
  id: string
  title: string
  categoryField: string
  categories: Array<CategoryConfig>
}

export interface CategoryConfig {
  id: string
  title: string
  password: {
    value: string
    label: string
    field: string
  } | null
  files: Array<FileItem>
}

export interface FileItem {
  id: string
  title: string
  url: string
  type: 'plan' | 'download'
}
