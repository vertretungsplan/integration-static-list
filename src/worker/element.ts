/*
 * vertretungsplan.io static list integration
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import basicAuth from 'basic-auth'
import { Router } from 'express'
import timeoutSignal from 'timeout-signal'
import { SchoolItem } from '../config/index.js'
import { capitalize } from '../util/capitalize.js'
import { sleep } from '../util/sleep.js'
import { crawl, CrawlerStatus, defaultCategory } from './crawl.js'

export class SchoolWorker {
  readonly item: SchoolItem

  lastPromise: Promise<CrawlerStatus>
  private lastSuccessPromise: Promise<CrawlerStatus>

  constructor (item: SchoolItem) {
    this.item = item

    const firstPromise = sleep(Math.random() * 1000 * 10 /* wait up to 10 seconds */)
      .then(() => this.doQuery())

    firstPromise.catch((ex) => {
      console.warn('initial query of ' + item.id + ' failed', ex)
    })

    this.lastPromise = firstPromise
    this.lastSuccessPromise = firstPromise

    // this should never happen, but the linter wants it
    this.crawlLoop().catch((ex) => console.warn('crawl loop crashed', ex))
  }

  createRouter (): Router {
    const router = Router()

    if (this.item.type === 1) {
      router.get('/config/default', (_, res) => {
        res.json({
          config: [],
          configValidationConditionId: '_true',
          contentBucketSets: [
            {
              id: 'default',
              usageConditionId: '_true',
              type: 'content'
            }
          ],
          conditionSets: []
        })
      })

      router.get('/content/default', (_, res, next) => {
        this.lastSuccessPromise.then((data) => {
          res.json(data.statusByCategory[defaultCategory].dataForClient)
        }).catch((ex) => next(ex))
      })
    } else if (this.item.type === 2) {
      const categories = this.item.categories
      const categoryField = this.item.categoryField

      if (categories.length !== 2) {
        throw new Error('only 2 categories are supported')
      }

      router.get('/config/default', (_, res) => {
        res.json({
          config: [
            ...categories.map((category) => ({
              param: categoryField,
              type: 'radio',
              value: category.id,
              label: category.title,
              visibilityConditionId: '_true'
            })),
            ...categories.filter((item) => !!item.password).map((category) => {
              if (!category.password) {
                throw new Error('illegal state')
              }

              return {
                param: category.password.field,
                type: 'password',
                visibilityConditionId: 'is' + capitalize(category.id),
                value: '',
                label: category.password.label
              }
            })
          ],
          configValidationConditionId: 'hasValidUserType',
          contentBucketSets: categories.map((category) => ({
            id: category.id,
            passwordParam: category.password ? category.password.field : undefined,
            usageConditionId: 'is' + capitalize(category.id),
            type: 'content'
          })),
          conditionSets: [
            ...categories.map((category) => ({
              id: 'is' + capitalize(category.id),
              type: 'paramIs',
              left: categoryField,
              right: category.id
            })),
            {
              id: 'hasValidUserType',
              type: 'or',
              left: 'is' + capitalize(categories[0].id),
              right: categories.length === 1 ? '_false' : 'is' + capitalize(categories[1].id)
            }
          ]
        })
      })

      router.get('/content/:category', (req, res, next) => {
        const categoryId = req.params.category
        const category = categories.find((item) => item.id === categoryId)

        if (!category) {
          res.sendStatus(404)

          return
        }

        if (category.password) {
          const auth = basicAuth(req)

          if ((!auth) || (auth.pass !== category.password.value)) {
            res.setHeader('WWW-Authenticate', 'Basic realm="Login"')
            res.sendStatus(401)
            return
          }
        }

        this.lastSuccessPromise.then((data) => {
          res.json(data.statusByCategory[categoryId].dataForClient)
        }).catch((ex) => next(ex))
      })
    } else {
      throw new Error('illegal state')
    }

    return router
  }

  private async crawlLoop () {
    try {
      await this.lastPromise
    } catch (ex) {
      // ignore
    }

    for (;;) {
      await sleep(1000 * 60 * 5 /* 5 minutes */)

      try {
        const newResponse = await this.doQuery()

        this.lastPromise = Promise.resolve(newResponse)
        this.lastSuccessPromise = Promise.resolve(newResponse)
      } catch (ex) {
        console.warn('crawling ' + this.item.id + ' failed', ex)

        this.lastPromise = Promise.reject(ex)

        // required to prevent an unhandled Promise rejection
        this.lastPromise.catch(() => null)
      }
    }
  }

  private async doQuery() {
    return await crawl(this.item, timeoutSignal(1000 * 60 * 10))
  }
}
