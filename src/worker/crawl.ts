/*
 * vertretungsplan.io static list integration
 * Copyright (C) 2019 - 2022 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import { CategoryConfig, SchoolItem } from '../config/index.js'
import { getUrlInfo } from './get-url-info.js'

export const defaultCategory = 'default'

export interface CrawlerStatus {
  statusByCategory: {[key: string]: CrawlerCategoryStatus}
}

interface CrawlerCategoryStatus {
  dataForClient: object
}

export async function crawl (item: SchoolItem, signal: AbortSignal): Promise<CrawlerStatus> {
  let categories: Array<CategoryConfig>

  if (item.type === 1) {
    categories = [
      {
        id: defaultCategory,
        title: defaultCategory,
        files: item.files,
        password: null
      }
    ]
  } else if (item.type === 2) {
    categories = item.categories
  } else {
    throw new Error('illegal state')
  }

  const categoriesWithContent = await Promise.all(categories.map(async (category) => {
    const itemsWithFileInfos = await Promise.all(category.files.map(async (file) => {
      const info = await getUrlInfo(file.url, signal)

      return {
        ...file,
        ...info
      }
    }))

    const dataForClient = {
      file: itemsWithFileInfos.map((item) => ({
        type: item.type,
        mimeType: item.mimeType,
        lastModified: item.lastModified,
        title: item.title,
        file: [
          {
            url: item.url,
            sha512: item.sha512,
            size: item.size
          }
        ],
        id: item.id,
        notify: item.type === 'plan'
      })),
      message: []
    }

    return { dataForClient, categoryId: category.id }
  }))

  const result: CrawlerStatus = {
    statusByCategory: {}
  }

  categoriesWithContent.forEach((category) => {
    result.statusByCategory[category.categoryId] = {
      dataForClient: category.dataForClient
    }
  })

  return result
}
